import org.junit.Before;
import org.junit.Test;

import java.util.Vector;

import static org.junit.Assert.*;

public class CompanyTest {
    private ResourcePoolUnderTest resourcePool;
    private Vector<Resource> resources = new Vector<Resource>();
    private int max_resources = 5;
    private CompanyUnderTest company;
    private Vector<Employee> employees = new Vector<Employee>();

    @Before
    public void setUp() {
        this.resources.add(new Resource(1000, "Monitor"));
        this.resources.add(new Resource(1200, "Monitor"));
//        this.resources.add(new Resource(900, "Case"));
//        this.resources.add(new Resource(300, "Keyboard"));
        this.resourcePool = new ResourcePoolUnderTest(this.resources, this.max_resources);
        this.company = new CompanyUnderTest(this.resourcePool);
        this.employees.add(new Employee("Ali"));
        this.employees.add(new Employee("Behrooz"));
        this.employees.add(new Employee("Catayon"));
        this.employees.add(new Employee("Dena"));
    }

    @Test
    public void hireEmployee() {
        Employee employee = this.employees.get(0);
        this.company.hireEmployee(employee);
        assertTrue(company.getEmployees().contains(employee));
        assertNotNull(employee.getResource());
    }

    @Test
    public void fireEmployee() {
        Employee employee = this.employees.get(0);
        this.company.hireEmployee(employee);
        this.company.fireEmployee(employee);
        assertFalse(company.getEmployees().contains(employee));
        assertNull(employee.getResource());
    }

    @Test
    public void goBankrupt() {
        int expected_value = 0;
        expected_value += this.resourcePool.getObjectPool().lastElement().getPrice();
        this.company.hireEmployee(this.employees.get(0));
        expected_value += this.resourcePool.getObjectPool().lastElement().getPrice();
        this.company.hireEmployee(this.employees.get(1));
        assertEquals(expected_value, this.company.goBankrupt());
        assertNull(this.employees.get(0).getResource());
        assertNull(this.employees.get(1).getResource());
    }
}