import org.junit.Before;
import org.junit.Test;

import java.util.Vector;

import static org.junit.Assert.*;

public class CompanyConstructorTest {
    private ResourcePoolUnderTest resourcePool;
    private Vector<Resource> resources = new Vector<Resource>();
    private int max_resources = 5;

    @Before
    public void setUp() {
        this.resources.add(new Resource(1000, "Monitor"));
        this.resources.add(new Resource(1200, "Monitor"));
        this.resources.add(new Resource(900, "Case"));
        this.resources.add(new Resource(300, "Keyboard"));
        this.resourcePool = new ResourcePoolUnderTest(this.resources, this.max_resources);
    }

    @Test
    public void company() {
        Company company = new CompanyUnderTest(this.resourcePool);
        assertNotNull(company);
        assertEquals(this.resourcePool, ((CompanyUnderTest) company).getResourcePool());
    }
}