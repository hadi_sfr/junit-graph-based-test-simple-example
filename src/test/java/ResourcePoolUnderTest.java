import java.util.Vector;

public class ResourcePoolUnderTest extends ResourcePool {
    public ResourcePoolUnderTest(Vector<Resource> object_pool, int max_resource_count) {
        super(object_pool, max_resource_count);
    }

    public Vector<Resource> getObjectPool() {
        return objectPool;
    }

    public int getMaxResourceCount() {
        return maxResourceCount;
    }
}
