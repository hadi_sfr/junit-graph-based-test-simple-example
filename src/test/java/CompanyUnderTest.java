import java.util.Vector;

public class CompanyUnderTest extends Company {

    public CompanyUnderTest(ResourcePool resourcePool) {
        super(resourcePool);
    }

    public Vector<Employee> getEmployees() {
        return employees;
    }

    public ResourcePool getResourcePool() {
        return resourcePool;
    }

    public static int getEkhtelasSize() {
        return EKHTELAS_SIZE;
    }
}
