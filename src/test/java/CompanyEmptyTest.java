import org.junit.Before;
import org.junit.Test;

import java.util.Vector;

import static org.junit.Assert.*;

public class CompanyEmptyTest {
    private ResourcePoolUnderTest resourcePool;
    private Vector<Resource> resources = new Vector<Resource>();
    private int max_resources = 5;
    private CompanyUnderTest company;
    private Vector<Employee> employees = new Vector<Employee>();

    @Before
    public void setUp() {
        this.resourcePool = new ResourcePoolUnderTest(this.resources, this.max_resources);
        this.company = new CompanyUnderTest(this.resourcePool);
    }

    @Test
    public void goBankrupt() {
        assertEquals(0, this.company.goBankrupt());
    }
}